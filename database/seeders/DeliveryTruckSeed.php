<?php

namespace Database\Seeders;

use App\Models\DeliveryTruck;
use App\Models\Tenant;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DeliveryTruckSeed extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tenant = Tenant::where('name', 'manager')->first();

        if ($tenant) {
            DeliveryTruck::updateOrCreate([
                'tenant_id' => $tenant->id,
                'truck_name' => 'truck1',
            ]);
        }
    }
}
