<?php

namespace Database\Seeders;

use App\Models\Tenant;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $tenant = Tenant::updateOrCreate([
            'name' => 'manager'
        ]);

        User::updateOrCreate([
            'tenant_id' => $tenant->id,
            'name' => 'manager',
            'email' => 'managerFuel@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password' => bcrypt('Hossein123456'),
            'role' => 'manager'
        ]);
    }
}
