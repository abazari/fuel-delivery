<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cargos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('delivery_truck_id')->references('id')->on('delivery_trucks');
            $table->foreignId('client_id')->references('id')->on('clients');
            $table->foreignId('tenant_id')->references('id')->on('tenants');
            $table->enum('status',['pending','Loading', 'in_transit','reject','finish'])->default('pending');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cargos');
    }
};
