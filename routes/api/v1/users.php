<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\V1\AuthController;
use App\Http\Controllers\Api\V1\TenantController;
use App\Http\Controllers\Api\V1\ClientController;
use App\Http\Controllers\Api\V1\OrderController;
use App\Http\Controllers\Api\V1\DeliveryTruckController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::group(['prefix' => 'v1/'], function () {
    Route::group(['prefix' => 'users/', 'as' => 'users.'], function () {

        Route::group(['prefix' => 'auth', 'as' => 'auth.'], function () {
            Route::post('login', [AuthController::class, 'login'])->name('login');
            Route::post('logout', [AuthController::class, 'logout'])->name('logout')->middleware('auth.user.only');
        });

        Route::resource('clients', ClientController::class, [
            'only' => ['store'],
        ]);

        Route::group(['prefix' => 'tenants', 'as' => 'tenant.'], function () {
            Route::get('select', [TenantController::class, 'select'])->name('select');
        });


        Route::group(['middleware' => ['auth:user']], function () {

            Route::group(['middleware' => ['role.manager']], function () {

                Route::resource('tenants', TenantController::class, [
                    'only' => ['index', 'store'],
                ]);

            });

            Route::resource('orders', OrderController::class, [
                'only' => ['index', 'store'],
            ]);

            Route::group(['prefix' => 'delivery-trucks', 'as' => 'truck.'], function () {
                Route::get('select', [DeliveryTruckController::class, 'select'])->name('select');
            });

            Route::resource('delivery-trucks', DeliveryTruckController::class, [
                'only' => ['index', 'store'],
            ]);

            Route::group(['prefix' => 'delivery-trucks', 'as' => 'truck.'], function () {
                Route::get('details/{id}', [DeliveryTruckController::class, 'details'])->name('details');
            });

            Route::group(['prefix' => 'clients', 'as' => 'clients.'], function () {
                Route::get('index', [ClientController::class, 'index'])->name('index');
            });

            Route::group(['prefix' => 'clients', 'as' => 'client.'], function () {
                Route::get('select', [ClientController::class, 'select'])->name('select');
            });

        });

    });
});
