<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | as the size rules. Feel free to tweak each of these messages here.
    |
    */

    'accepted' => 'The :attribute must be accepted.',
    'accepted_if' => 'The :attribute must be accepted when :other is :value.',
    'active_url' => 'The :attribute is not a valid URL.',
    'after' => 'The :attribute must be a date after :date.',
    'after_or_equal' => 'The :attribute must be a date after or equal to :date.',
    'alpha' => 'The :attribute must only contain letters.',
    'alpha_dash' => 'The :attribute must only contain letters, numbers, dashes and underscores.',
    'alpha_num' => 'The :attribute must only contain letters and numbers.',
    'array' => 'The :attribute must be an array.',
    'ascii' => 'The :attribute must only contain single-byte alphanumeric characters and symbols.',
    'before' => 'The :attribute must be a date before :date.',
    'before_or_equal' => 'The :attribute must be a date before or equal to :date.',
    'between' => [
        'array' => 'The :attribute must have between :min and :max items.',
        'file' => 'The :attribute must be between :min and :max kilobytes.',
        'numeric' => 'The :attribute must be between :min and :max.',
        'string' => 'The :attribute must be between :min and :max characters.',
    ],
    'boolean' => 'The :attribute field must be true or false.',
    'confirmed' => 'The :attribute confirmation does not match.',
    'current_password' => 'The password is incorrect.',
    'date' => 'The :attribute is not a valid date.',
    'date_equals' => 'The :attribute must be a date equal to :date.',
    'date_format' => 'The :attribute does not match the format :format.',
    'decimal' => 'The :attribute must have :decimal decimal places.',
    'declined' => 'The :attribute must be declined.',
    'declined_if' => 'The :attribute must be declined when :other is :value.',
    'different' => 'The :attribute and :other must be different.',
    'digits' => 'The :attribute must be :digits digits.',
    'digits_between' => 'The :attribute must be between :min and :max digits.',
    'dimensions' => 'The :attribute has invalid image dimensions.',
    'distinct' => 'The :attribute field has a duplicate value.',
    'doesnt_end_with' => 'The :attribute may not end with one of the following: :values.',
    'doesnt_start_with' => 'The :attribute may not start with one of the following: :values.',
    'email' => 'The :attribute must be a valid email address.',
    'ends_with' => 'The :attribute must end with one of the following: :values.',
    'enum' => 'The selected :attribute is invalid.',
    'exists' => 'The selected :attribute is invalid.',
    'file' => 'The :attribute must be a file.',
    'filled' => 'The :attribute field must have a value.',
    'gt' => [
        'array' => 'The :attribute must have more than :value items.',
        'file' => 'The :attribute must be greater than :value kilobytes.',
        'numeric' => 'The :attribute must be greater than :value.',
        'string' => 'The :attribute must be greater than :value characters.',
    ],
    'gte' => [
        'array' => 'The :attribute must have :value items or more.',
        'file' => 'The :attribute must be greater than or equal to :value kilobytes.',
        'numeric' => 'The :attribute must be greater than or equal to :value.',
        'string' => 'The :attribute must be greater than or equal to :value characters.',
    ],
    'image' => 'The :attribute must be an image.',
    'in' => 'The selected :attribute is invalid.',
    'in_array' => 'The :attribute field does not exist in :other.',
    'integer' => 'The :attribute must be an integer.',
    'ip' => 'The :attribute must be a valid IP address.',
    'ipv4' => 'The :attribute must be a valid IPv4 address.',
    'ipv6' => 'The :attribute must be a valid IPv6 address.',
    'json' => 'The :attribute must be a valid JSON string.',
    'lowercase' => 'The :attribute must be lowercase.',
    'lt' => [
        'array' => 'The :attribute must have less than :value items.',
        'file' => 'The :attribute must be less than :value kilobytes.',
        'numeric' => 'The :attribute must be less than :value.',
        'string' => 'The :attribute must be less than :value characters.',
    ],
    'lte' => [
        'array' => 'The :attribute must not have more than :value items.',
        'file' => 'The :attribute must be less than or equal to :value kilobytes.',
        'numeric' => 'The :attribute must be less than or equal to :value.',
        'string' => 'The :attribute must be less than or equal to :value characters.',
    ],
    'mac_address' => 'The :attribute must be a valid MAC address.',
    'max' => [
        'array' => 'The :attribute must not have more than :max items.',
        'file' => 'The :attribute must not be greater than :max kilobytes.',
        'numeric' => 'The :attribute must not be greater than :max.',
        'string' => 'The :attribute must not be greater than :max characters.',
    ],
    'max_digits' => 'The :attribute must not have more than :max digits.',
    'mimes' => 'The :attribute must be a file of type: :values.',
    'mimetypes' => 'The :attribute must be a file of type: :values.',
    'min' => [
        'array' => 'The :attribute must have at least :min items.',
        'file' => 'The :attribute must be at least :min kilobytes.',
        'numeric' => 'The :attribute must be at least :min.',
        'string' => 'The :attribute must be at least :min characters.',
    ],
    'min_digits' => 'The :attribute must have at least :min digits.',
    'multiple_of' => 'The :attribute must be a multiple of :value.',
    'not_in' => 'The selected :attribute is invalid.',
    'not_regex' => 'The :attribute format is invalid.',
    'numeric' => 'The :attribute must be a number.',
    'password' => [
        'letters' => 'The :attribute must contain at least one letter.',
        'mixed' => 'The :attribute must contain at least one uppercase and one lowercase letter.',
        'numbers' => 'The :attribute must contain at least one number.',
        'symbols' => 'The :attribute must contain at least one symbol.',
        'uncompromised' => 'The given :attribute has appeared in a data leak. Please choose a different :attribute.',
    ],
    'present' => 'The :attribute field must be present.',
    'prohibited' => 'The :attribute field is prohibited.',
    'prohibited_if' => 'The :attribute field is prohibited when :other is :value.',
    'prohibited_unless' => 'The :attribute field is prohibited unless :other is in :values.',
    'prohibits' => 'The :attribute field prohibits :other from being present.',
    'regex' => 'The :attribute format is invalid.',
    'required' => 'The :attribute field is required.',
    'required_array_keys' => 'The :attribute field must contain entries for: :values.',
    'required_if' => 'The :attribute field is required when :other is :value.',
    'required_if_accepted' => 'The :attribute field is required when :other is accepted.',
    'required_unless' => 'The :attribute field is required unless :other is in :values.',
    'required_with' => 'The :attribute field is required when :values is present.',
    'required_with_all' => 'The :attribute field is required when :values are present.',
    'required_without' => 'The :attribute field is required when :values is not present.',
    'required_without_all' => 'The :attribute field is required when none of :values are present.',
    'same' => 'The :attribute and :other must match.',
    'size' => [
        'array' => 'The :attribute must contain :size items.',
        'file' => 'The :attribute must be :size kilobytes.',
        'numeric' => 'The :attribute must be :size.',
        'string' => 'The :attribute must be :size characters.',
    ],
    'starts_with' => 'The :attribute must start with one of the following: :values.',
    'string' => 'The :attribute must be a string.',
    'timezone' => 'The :attribute must be a valid timezone.',
    'unique' => 'The :attribute has already been taken.',
    'uploaded' => 'The :attribute failed to upload.',
    'uppercase' => 'The :attribute must be uppercase.',
    'url' => 'The :attribute must be a valid URL.',
    'ulid' => 'The :attribute must be a valid ULID.',
    'uuid' => 'The :attribute must be a valid UUID.',
    'strong_password' => 'The :attribute must contain at least one uppercase letter, one lowercase letter, and one digit.',
    'no_sql_injection' => 'The :attribute field contains a SQL injection keyword.',
    'strip_tags' => 'The :attribute field contains HTML tags.',
    'check_admin_with_user_id' => 'The selected :attribute is invalid.',
    'phone' => 'The :attribute must be a valid phone number.',
    'unable_to_delete' => 'Unable to delete :model with id: :ids',
    'exists_admin_job' => 'The selected :attribute is invalid.',
    'order_admin_match' =>  'The selected :attribute is invalid because the order does not belong to the specified administrator.',
    'order_user_match' =>  'The selected :attribute is invalid because the order does not belong to the specified User.',
    'double' => 'The :attribute field must be a double value with up to 2 decimal places.',
    "decimal_up_to_6" => "The :attribute field must be a number with up to 6 decimal places.",
    'is_user_synced_with_admin' => 'The :attribute must be synced with the specified admin.',
    'zarinpal_merchant_id' => 'The :attribute must be a valid Zarinpal merchant ID.',
    'order_amount_min' => 'The order amount cannot be less than :percent%.',
    'be_super_display' => 'You cannot use :attribute in this case. Your user must be a super user.',
    'has_user' => ':attribute must have user role.',
    'not_self_user_id' => 'The selected :attribute is invalid.',
    'not_self_admin_id' => 'The selected :attribute is invalid.',
    'cannot_use_attribute' => "You can't use this :attribute",


    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => [
        'attribute-name' => [
            'rule-name' => 'custom-message',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap our attribute placeholder
    | with something more reader friendly such as "E-Mail Address" instead
    | of "email". This simply helps us make our message more expressive.
    |
    */

    'attributes' => [
        'mobile' => 'Mobile',
        'email' => 'Email',
        'otp_status' => 'otp status',
        'password' => 'Password',
        'name' => 'Name',
        'address' => 'Address',
        'gender' => 'Gender',
        'national_code' => 'National Code',
        'full_name' => 'Full Name',
        'image' => 'Image',
        'login_type' => 'Login Type',
        'code_mobile' => 'Code Mobile',
        'code_email' => 'Code Email',
        'orders' => 'Orders',
        'order' => 'Order',
        'jobs' => 'Job',
        'admin_id' => 'Admin ID',
        'title' => 'Title',
        'request_delivery_at' => 'Delivery Request Date',
        'context' => 'Content',
        'type' => 'Type',
        'number' => 'Number',
        'orderable_id' => 'Orderable ID',
        'services' => 'Services',
        'user' => 'User',
        'users' => 'Users',
        'job' => 'Job',
        'admins' => 'Admins',
        'job_types' => 'Job Types',
        'profile' => 'Profile',
        'lang' => 'Language',
        'user_id' => 'User Id',
        'item' => 'Item',
        'invoice' => 'Invoice',
        'invoices' => 'Invoices',
        'order_id' => 'Order Id',
        'code_factor' => 'Invoice Code',
        'invoice_details' => 'Invoice Details',
        'payment' => 'Payment',
        'invoice_id' => 'Invoice Id',
        'merchant_id' => 'Merchant Id',
        'transactions' => 'Transaction',
        'service' => 'Service',
        'active' => 'Active',
        'de_active' => 'De Active',
        'product' => 'Product',
        'products' => 'Products',
        'code' => 'The Code',
        'link' => 'The Link',
        'regions' => 'Regions',
        'job_ad' => 'Job Ad',
        'post' => 'Post',
        'posts' => 'Posts',

        'user_follow_id' => 'User Follow Id',
        'admin_follow_id' => 'Admin Follow Id',
        'user_follow' => 'User Follow',
        'user_followings' => 'User Following',
        'admin_follow' => 'Admin Follow',
        'user_name' => 'User Name (mobile or email)',
        'ticket' => 'Ticket',
        'ticket_details' => 'Ticket Details',

        'post_type' => 'Post Type',
        'images' => 'Images',
        'client' => 'Client',
        'tenant' => 'Tenant',
        'delivery_truck' => 'Delivery Truck',
        'cargo' => 'Cargo',

    ],

];
