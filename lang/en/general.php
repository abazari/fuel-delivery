<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Language
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */


    'attributes' => [
        'thanks' => 'Thanks',
        'email_verification' => 'Email Verification'
    ],
    'exception' => [
        'valid_image' => 'The provided file is not a valid image file.',
        'valid_file' => 'The provided file is not a valid file.',
        'is_problem' => 'Sorry, an error has occurred! Please contact the support team.',
        'update_unsuccessful' => 'Update :model was unsuccessful',
        'dont_have_response' => 'The response is empty.',
        'ad_creation_limit_reached' => 'Ad creation limit reached for the admin.',
        'type_attribute' => 'The selected :attribute is invalid.',
        'gateway_failed' => 'You encountered an issue while trying to access the payment gateway. Please contact support.'
    ],

    'messages' => [
        'success' => ':item has been successfully saved',
        'edit' => ':item has been edited saved',
        'show' => 'Show :item',
        'update' => 'Updated :item',
        'cancel' => 'Canceled :item',
        'delete' => 'Deleted :item',
        'check' => 'Checked :item',
        'toggle' => ':item :toggle شد.',
        'vip_post' => 'Vip Posts',
        'enter_gateway' => 'Enter Payment Gateway',
        'error' => 'Error in :error'

    ],
    'order' => [
        'pending' => 'Pending',
        'doing' => 'Doing',
        'sending' => 'Sending',
        'finished' => 'Finished',
        'canceled' => 'Canceled',

        'not_exist' => 'The order does not exist!',
        'text_status' => 'Your order status has been changed to :status.'

    ],
    'state' => [
        'pending' => 'Pending',
        'reject' => 'Rejected',
        'approve' => 'Approved',
    ],
    'model' => [
        'not_exist' => 'The requested :model does not exist'
    ],

    'invalid' => ':attribute selected is invalid.',
    'client' => [
        'store'
    ]
];
