<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'before_continuing' => 'You need to sign in before continuing.',
    'login' => 'You have successfully logged in',
    'logout' => 'You have successfully logout',
    'could_not_create_token' => 'Unfortunately, the token was not created!',
    'invalid_credentials' => 'Username or password is incorrect.',
    'access_denied' => "You are already logged in and don't have permission to access this page.",
    'access_denied_super' => 'You are not authorized to access this page as a super user. Please log in as a super user to access this page.',
    'unverified_email' => 'Your email is not verified.',
    'unverified_mobile' => 'Your mobile is not verified.',
    'user_authenticated' => 'User is already authenticated.',
    'access' => 'You do not have access to this panel.',
    'is_authenticated' => 'You are already logged in',
    'complete_profile' => 'You must complete your profile before continuing.',
    'register' => [
        'success' => "Your registration was successful, and an email has been sent to you.",
        'resend' => "The link has been resent to your email."
    ],
    'status_username' => [
        'email' => 'A verification code has been sent to the email address with status :status.',
        'mobile' => 'A verification code has been sent to the mobile number with status :status.'
    ]
];
