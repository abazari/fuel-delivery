<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderStoreResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->order->id,
            'user' => $this->order->user->name,
            'client' => $this->order->client->client_name,
            'tenant' => $this->order->tenant->name,
            'amount' => $this->order->fuel_amount,
            'truck_name' => $this->cargo->deliveryTruck->truck_name,
            'status_delivery' => $this->cargo->pending
        ];
    }
}
