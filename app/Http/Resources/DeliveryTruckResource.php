<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class DeliveryTruckResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'truck_name' => $this->truck_name,
            'license_plate' => $this->license_plate,
            'model' => $this->model,
            'tenant' => $this->tenant->name
        ];
    }
}
