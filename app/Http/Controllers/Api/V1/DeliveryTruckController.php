<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\DeliveryTruckStoreRequest;
use App\Http\Resources\DeliveryTruckResource;
use App\Http\Resources\DeliveryTruckStoreResource;
use App\Http\Resources\PaginationResource;
use App\Http\Resources\SelectTruckResource;
use App\Http\Resources\CargoResource;
use App\Repositories\DeliveryTruck\DeliveryTruckRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

/**
 * Class DeliveryTruckController
 *
 * This class handles operations related to delivery trucks.
 */
class DeliveryTruckController extends Controller
{
    /**
     * DeliveryTruckController constructor.
     *
     * @param DeliveryTruckRepositoryInterface $deliveryTruckRepository The delivery truck repository.
     */
    public function __construct(protected DeliveryTruckRepositoryInterface $deliveryTruckRepository)
    {
    }

    /**
     * Display a listing of delivery trucks for the authenticated user's tenant.
     *
     * @return JsonResponse The JSON response containing the list of delivery trucks.
     */
    public function select(): JsonResponse
    {
        $user = Auth::guard('user')->user();

        // Get all delivery trucks for the user's tenant
        $deliveryTrucks = $this->deliveryTruckRepository->getAll($user->tenant_id);

        return $this->success(
            __('general.messages.show', [
                'item' => __('validation.attributes.delivery_truck')
            ]),
            SelectTruckResource::collection($deliveryTrucks)
        );

    }

    /**
     * Display a listing of delivery trucks with pagination.
     *
     * @return JsonResponse The JSON response containing the list of delivery trucks and pagination information.
     */
    public function index(): JsonResponse
    {
        try {
            $user =Auth::guard('user')->user();

            $tenants = $this->deliveryTruckRepository->getAllWithPagination($user->tenant_id);

            return $this->success(__('general.messages.show', [
                'item' => __('validation.attributes.delivery_truck')
            ]), [
                'delivery_truck' => DeliveryTruckResource::collection($tenants),
                'pagination' => new PaginationResource($tenants),
            ]);

        } catch (\Exception $e) {
            return $this->error(__('general.messages.error', [
                'item' => __('validation.attributes.delivery_truck')
            ]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Store a new delivery trucks along with user information.
     *
     * @param DeliveryTruckStoreRequest $request The request containing delivery trucks data.
     *
     * @return JsonResponse The JSON response indicating success or failure.
     */
    public function store(DeliveryTruckStoreRequest $request): JsonResponse
    {
        try {
            $data = $request->all();

            $tenant = $this->deliveryTruckRepository->createDeliveryTruck($data);


            $responseData = new DeliveryTruckStoreResource($tenant);

            return $this->success(__('general.messages.success', [
                'item' => __('validation.attributes.delivery_truck')
            ]), $responseData);

        } catch (\Exception $e) {
            return $this->error(__('general.messages.error', [
                'item' => __('validation.attributes.delivery_truck')
            ]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Get details of a delivery truck, including its cargos.
     *
     * @param int $id The ID of the delivery truck.
     *
     * @return \Illuminate\Http\JsonResponse The JSON response with truck and cargo details.
     */
    public function details($id)
    {
        $truck = $this->deliveryTruckRepository->findTruckWithId($id);

        // Check if the truck exists.
        if (! $truck) {
            // If the truck is not found, return a 404 Not Found error with a relevant error message.
            return $this->error(__('general.model.not_exist', [
                'model' => __('validation.attributes.delivery_truck')
            ]), Response::HTTP_NOT_FOUND);
        }

        // Retrieve cargos associated with the truck.
        $cargos = $truck->cargos()->get();

        // Return a success response with cargo and truck details.
        return $this->success(__('general.messages.success', [
            'item' => __('validation.attributes.cargo')
        ]), [
            'cargos' => CargoResource::collection($cargos),
            'truck' => DeliveryTruckResource::make($truck),
        ]);
    }

}

