<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\ClientStoreRequest;
use App\Http\Resources\ClientResource;
use App\Http\Resources\ClientStoreResource;
use App\Http\Resources\PaginationResource;
use App\Http\Resources\SelectClientResource;
use App\Models\Client;
use App\Repositories\Client\ClientRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function __construct(protected ClientRepositoryInterface $clientRepository)
    {

    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = Auth::guard('user')->user();

        $clients = $this->clientRepository->getAllWithPagination($user->client_id);

        return $this->success(
            __('general.messages.show', [
                'item' => __('validation.attributes.order')
            ]),
            [
                'clients' => ClientResource::collection($clients),
                'pagination' => new PaginationResource($clients),
            ]
        );
    }

    /**
     * Retrieve all client from the database and return a JSON response.
     *
     * @return JsonResponse
     */
    public function select(): JsonResponse
    {
        $user = Auth::guard('user')->user();

        $tenants = $this->clientRepository->getAll($user->tenant_id);

        $responseResource = SelectClientResource::collection($tenants);

        return $this->success(__('general.messages.show', [
            'item' => __('validation.attributes.tenant')
        ]), $responseResource);
    }


    /**
     * Store a new client.
     *
     * @param ClientStoreRequest $request
     * @return JsonResponse
     */
    public function store(ClientStoreRequest $request): JsonResponse
    {
        try {

            // Retrieve all input data from the request
            $data = $request->all();

            $client = $this->clientRepository->createClient($data);

            // Return a success response with a success message and the newly created client resource
            return $this->success(__('general.messages.success', [
                'item' => __('validation.attributes.client')
            ]), ClientStoreResource::make($client));

        } catch (\Exception $e) {
            dd($e);
            return $this->error(__('general.messages.error', [
                'item' => __('validation.attributes.client')
            ]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Client $client)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Client $client)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Client $client)
    {
        //
    }
}
