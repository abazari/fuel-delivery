<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\AuthLoginRequest;
use App\Http\Resources\LoginResource;
use App\Repositories\User\UserRepositoryInterface;
use App\Traits\LoginTrait;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

/**
 * Class AuthController
 *
 * This class handles authentication-related operations for the API.
 */
class AuthController extends Controller
{
    use LoginTrait;

    /**
     * AuthController constructor.
     *
     * @param UserRepositoryInterface $userRepository The user repository interface.
     */
    public function __construct(protected UserRepositoryInterface $userRepository)
    {
    }

    /**
     * Handle user login.
     *
     * @param AuthLoginRequest $request The login request.
     *
     * @return JsonResponse The JSON response.
     */
    public function login(AuthLoginRequest $request): JsonResponse
    {
        try {

            // Attempt to log in and get the access token with jwt
            if (! $accessToken = $this->attemptLogin($request, 'user', $request->status)) {
                return $this->error(__('auth.invalid_credentials'), Response::HTTP_UNAUTHORIZED);
            }

            $user = $this->userRepository->findEmail($request->email);

            // Prepare data for the response
            $data = new LoginResource((object)[
                'access_token' => $accessToken,
                'user' => $user,
            ]);

            return $this->success(__('auth.login'), $data);

        } catch (\Exception $e) {
            return $this->error(__('auth.could_not_create_token'), Response::HTTP_UNAUTHORIZED);
        }
    }

    /**
     * logout
     *
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        // Call the internal method to invalidate the token.
        $this->performLogout();

        // Return a success response.
        return $this->success(__('auth.logout'), true);
    }
}
