<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\OrderStoreRequest;
use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderStoreResource;
use App\Http\Resources\PaginationResource;
use App\Models\Cargo;
use App\Models\Order;
use App\Repositories\Cargo\CargoRepositoryInterface;
use App\Repositories\Order\OrderRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    /**
     * @param OrderRepositoryInterface $orderRepository
     * @param CargoRepositoryInterface $cargoRepository
     */
    public function __construct(protected OrderRepositoryInterface $orderRepository,
                                protected CargoRepositoryInterface $cargoRepository)
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $user = Auth::guard('user')->user();

        // Retrieve orders with pagination for the user's tenant
        $orders = $this->orderRepository->getAllWithPagination($user->tenant_id);

        return $this->success(
            __('general.messages.show', [
                'item' => __('validation.attributes.order')
            ]),
            [
                'orders' => OrderResource::collection($orders),
                'pagination' => new PaginationResource($orders),
            ]
        );
    }



    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a new order along with associated cargo information.
     *
     * @param OrderStoreRequest $request The request containing order data.
     *
     * @return JsonResponse The JSON response indicating success or failure.
     */
    public function store(OrderStoreRequest $request): JsonResponse
    {
        try {
            $data = $request->all();

            $user = Auth::guard('user')->user();

            // Add user_id and tenant_id to the order data
            $data['user_id'] = $user->id;
            $data['tenant_id'] = $user->tenant_id;

            // Create a new order using the OrderRepository
            $order = $this->orderRepository->createOrder($data);
            // Create a new cargo using the CargoRepository
            $cargo = $this->cargoRepository->createCargo($data);

            $responseData = new OrderStoreResource((object)[
                'order' => $order,
                'cargo' => $cargo,
            ]);

            return $this->success(__('general.messages.success', [
                'item' => __('validation.attributes.order')
            ]), $responseData);

        } catch (\Exception $e) {
            return $this->error(__('general.messages.error', [
                'item' => __('validation.attributes.order')
            ]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Order $order)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Order $order)
    {
        //
    }
}
