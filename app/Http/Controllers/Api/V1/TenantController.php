<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\TenantStoreRequest;
use App\Http\Resources\PaginationResource;
use App\Http\Resources\SelectResource;
use App\Http\Resources\TenantResource;
use App\Http\Resources\TenantStoreResource;
use App\Models\Tenant;
use App\Repositories\Tenant\TenantRepositoryInterface;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class TenantController extends Controller
{
    /**
     * TenantController constructor.
     *
     * @param TenantRepositoryInterface $tenantRepository The tenant repository.
     * @param UserRepositoryInterface $userRepository The tenant repository.
     */
    public function __construct(protected TenantRepositoryInterface $tenantRepository,
    protected UserRepositoryInterface $userRepository)
    {

    }

    /**
     * Display a listing of tenants with pagination.
     *
     * @return JsonResponse The JSON response containing the list of tenants and pagination information.
     */
    public function index(): JsonResponse
    {
        try {
            // Get all tenants with pagination
            $tenants = $this->tenantRepository->getAllWithPagination();

            return $this->success(__('general.messages.show', [
                'item' => __('validation.attributes.tenant')
            ]), [
                'tenant' => TenantResource::collection($tenants),
                'pagination' => new PaginationResource($tenants),
            ]);

        } catch (\Exception $e) {
            return $this->error(__('general.messages.error', [
                'item' => __('validation.attributes.tenant')
            ]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Retrieve all tenants from the database and return a JSON response.
     *
     * @return JsonResponse
     */
    public function select(): JsonResponse
    {
        // Query the database to get all tenants
        $tenants = $this->tenantRepository->getAll();

        // Prepare data for response using SelectResource
        $responseResource = SelectResource::collection($tenants);

        // Return a success response with a message indicating successful retrieval of tenants
        return $this->success(__('general.messages.show', [
            'item' => __('validation.attributes.tenant')
        ]), $responseResource);
    }


    /**
     * Store a new tenant along with user information.
     *
     * @param TenantStoreRequest $request The request containing tenant data.
     *
     * @return JsonResponse The JSON response indicating success or failure.
     */
    public function store(TenantStoreRequest $request): JsonResponse
    {
        try {
            $data = $request->all();

            $tenant = $this->tenantRepository->createTenant($data);

            // Add the tenant ID to the user data
            $data['tenant_id'] = $tenant->id;

            $user = $this->userRepository->createUser($data);

            $responseData = new TenantStoreResource((object)[
                'user' => $user,
                'tenant' => $tenant,
            ]);

            return $this->success(__('general.messages.success', [
                'item' => __('validation.attributes.tenant')
            ]), $responseData);

        } catch (\Exception $e) {
            return $this->error(__('general.messages.error', [
                'item' => __('validation.attributes.tenant')
            ]), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }


    /**
     * Display the specified resource.
     */
    public function show(Tenant $tenant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Tenant $tenant)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Tenant $tenant)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Tenant $tenant)
    {
        //
    }
}
