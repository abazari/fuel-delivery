<?php

namespace App\Http\Middleware;

use App\Traits\ResponsibleTrait;
use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthUserMiddleware
{
    use ResponsibleTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if (!\Auth::guard('user')->check()){
             return $this->error(__('auth.before_continuing'),Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
