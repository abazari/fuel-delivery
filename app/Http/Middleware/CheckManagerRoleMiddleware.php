<?php

namespace App\Http\Middleware;

use App\Traits\ResponsibleTrait;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CheckManagerRoleMiddleware
{
    use ResponsibleTrait;
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        $user = Auth::guard('user')->user();

        if ($user->role != config('config.role.manager')) {
            return $this->error(__('auth.not_role_manager'), Response::HTTP_FORBIDDEN);
        }

        return $next($request);
    }
}
