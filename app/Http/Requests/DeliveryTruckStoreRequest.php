<?php

namespace App\Http\Requests;

use App\Rules\NoSqlInjection;
use App\Rules\StrongPassword;
use Illuminate\Foundation\Http\FormRequest;

class DeliveryTruckStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'truck_name' => ['required', 'string', new NoSqlInjection()],
            'tenant_id' => ['required', 'exists:tenants,id'],
            'license_plate' => ['nullable', 'string', new NoSqlInjection()],
            'model' => ['nullable', 'string', new NoSqlInjection()],
        ];
    }
}
