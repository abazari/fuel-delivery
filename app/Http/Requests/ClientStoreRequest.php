<?php

namespace App\Http\Requests;

use App\Rules\NoSqlInjection;
use App\Rules\ScriptTags;
use Illuminate\Foundation\Http\FormRequest;

class ClientStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'client_name' => ['required', 'max:130', 'string', new NoSqlInjection()],
            'address' => ['required', 'string', 'max:1000', new ScriptTags(), new NoSqlInjection()],
            'mobile' => ['nullable', 'regex:/^([0-9\s\-\+\(\)]*)$/', 'digits:11'],
            'tenant_id' => ['required','exists:tenants,id']

        ];
    }
}
