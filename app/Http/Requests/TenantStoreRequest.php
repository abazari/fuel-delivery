<?php

namespace App\Http\Requests;

use App\Rules\NoSqlInjection;
use App\Rules\StrongPassword;
use Illuminate\Foundation\Http\FormRequest;

class TenantStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', new NoSqlInjection()],
            'domain' => ['nullable', 'string', new NoSqlInjection()],
            'email' => ['required','email'],
            'password' => ['required', 'confirmed',new StrongPassword(), 'min:8',],
        ];
    }
}
