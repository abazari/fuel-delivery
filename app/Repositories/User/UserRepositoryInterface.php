<?php

namespace App\Repositories\User;

use App\Models\User;

interface UserRepositoryInterface
{
    /**
     * Find a user by email.
     *
     * @param string $email The email to search for.
     *
     * @return User|null The User model instance or null if not found.
     */
    public function findEmail(string $email): ?User;

    /**
     * Create a new user record.
     *
     * @param array $data The data for creating a user.
     *
     * @return User The newly created User model instance.
     */
    public function createUser(array $data): User;

}