<?php

namespace App\Repositories\User;

use App\Models\User;
use Carbon\Carbon;

/**
 * Class UserRepository
 *
 * This class represents the repository for User model operations.
 */
class UserRepository implements UserRepositoryInterface
{
    /**
     * UserRepository constructor.
     *
     * @param User $user The User model instance.
     */
    public function __construct(protected User $user)
    {
    }

    /**
     * Find a user by email.
     *
     * @param string $email The email to search for.
     *
     * @return User|null The User model instance or null if not found.
     */
    public function findEmail(string $email): ?User
    {
        return $this->user->whereEmail($email)->first();
    }

    /**
     * Create a new user record.
     *
     * @param array $data The data for creating a user.
     *
     * @return User The newly created User model instance.
     */
    public function createUser(array $data): User
    {
        return $this->user->create([
            'tenant_id' => $data['tenant_id'],
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
