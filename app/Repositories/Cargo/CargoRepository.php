<?php

namespace App\Repositories\Cargo;

use App\Models\Cargo;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class CargoRepository
 *
 * This class represents the repository for Cargo model operations.
 */
class CargoRepository implements CargoRepositoryInterface
{
    /**
     * CargoRepository constructor.
     *
     * @param Cargo $Cargo The Cargo model instance.
     */
    public function __construct(protected Cargo $cargo)
    {
    }

    /**
     * Create a new Cargo record.
     *
     * @param array $data The data for creating a Cargo.
     *
     * @return Cargo The newly created Cargo model instance.
     */
    public function createCargo(array $data): Cargo
    {
        return $this->cargo->create([
            'delivery_truck_id' => $data['delivery_truck_id'],
            'client_id' => $data['client_id'],
            'tenant_id' => $data['tenant_id'],
        ]);
    }



}
