<?php

namespace App\Repositories\Cargo;

use App\Models\Cargo;
use App\Models\DeliveryTruck;
use App\Models\Tenant;
use Illuminate\Pagination\LengthAwarePaginator;

interface CargoRepositoryInterface
{
    /**
     * Create a new cargo record.
     *
     * @param array $data The data for creating a cargo.
     *
     * @return Tenant The newly created Cargo model instance.
     */
    public function createCargo(array $data): Cargo;

}
