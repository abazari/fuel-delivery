<?php

namespace App\Repositories\Client;

use App\Models\Client;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface ClientRepositoryInterface
{
    /**
     * Create a new client record.
     *
     * @param array $data The data for creating a client.
     *
     * @return Client The newly created Client model instance.
     */
    public function createClient(array $data): Client;

    /**
     * Get all client with optional pagination.
     *
     * @param int|null $perPage The number of items per page for pagination.
     *
     * @return LengthAwarePaginator The paginated result of all client.
     */
    public function getAllWithPagination(int $clientId,?int $perPage = null): LengthAwarePaginator;

    /**
     * Get all client for a specific client.
     *
     *
     * @return Collection The collection of client for the given client.
     */
    public function getAll(int $tenantId): Collection;
}
