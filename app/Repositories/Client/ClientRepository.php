<?php

namespace App\Repositories\Client;

use App\Models\Client;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class ClientRepository
 *
 * This class represents the repository for Client model operations.
 */
class ClientRepository implements ClientRepositoryInterface
{
    /**
     * ClientRepository constructor.
     *
     * @param Client $client The Client model instance.
     */
    public function __construct(protected Client $client)
    {
    }

    /**
     * Create a new client record.
     *
     * @param array $data The data for creating a client.
     *
     * @return Client The newly created Client model instance.
     */
    public function createClient(array $data): Client
    {
        return $this->client->create([
            'client_name' => $data['client_name'],
            'address' => $data['address'],
            'mobile' => $data['mobile'],
            'tenant_id' => $data['tenant_id'],
        ]);
    }

    /**
     * Get all client with optional pagination.
     *
     * @param int|null $perPage The number of items per page for pagination.
     *
     * @return LengthAwarePaginator The paginated result of all client.
     */
    public function getAllWithPagination(int $clientId,?int $perPage = null): LengthAwarePaginator
    {
        $query = $this->client->query()
            ->latest()
        ->where('client_id',$clientId);

        $result = $query->paginate($perPage ?? config('config.default_paginate'));

        return $result;

    }

    /**
     * Get all client for a specific client.
     *
     *
     * @return Collection The collection of client for the given client.
     */
    public function getAll(int $tenantId): Collection
    {
        $query = $this->client
            ->latest()
        ->where('tenant_id',$tenantId);

        $result = $query->get();

        return $result;
    }
}
