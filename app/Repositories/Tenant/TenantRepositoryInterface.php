<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface TenantRepositoryInterface
{
    /**
     * Create a new tenant record.
     *
     * @param array $data The data for creating a client.
     *
     * @return Tenant The newly created Client model instance.
     */
    public function createTenant(array $data): Tenant;

    /**
     * Get all tenants with optional pagination.
     *
     * @param int|null $perPage The number of items per page for pagination.
     *
     * @return LengthAwarePaginator The paginated result of all tenants.
     */
    public function getAllWithPagination(?int $perPage = null): LengthAwarePaginator;

    /**
     * Get all delivery trucks for a specific tenant.
     *
     *
     * @return Collection The collection of delivery trucks for the given tenant.
     */
    public function getAll(): Collection;
}
