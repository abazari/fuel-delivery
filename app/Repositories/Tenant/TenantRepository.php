<?php

namespace App\Repositories\Tenant;

use App\Models\Tenant;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class ClientRepository
 *
 * This class represents the repository for Client model operations.
 */
class TenantRepository implements TenantRepositoryInterface
{
    /**
     * ClientRepository constructor.
     *
     * @param Client $client The Client model instance.
     */
    public function __construct(protected Tenant $tenant)
    {
    }

    /**
     * Create a new tenant record.
     *
     * @param array $data The data for creating a client.
     *
     * @return Tenant The newly created Client model instance.
     */
    public function createTenant(array $data): Tenant
    {
        return $this->tenant->create([
            'name' => $data['name'],
            'domain' => $data['domain'],
        ]);
    }

    /**
     * Get all tenants with optional pagination.
     *
     * @param int|null $perPage The number of items per page for pagination.
     *
     * @return LengthAwarePaginator The paginated result of all tenants.
     */
    public function getAllWithPagination(?int $perPage = null): LengthAwarePaginator
    {
        $query = $this->tenant->query()->latest();

        $result = $query->paginate($perPage ?? config('config.default_paginate'));

        return $result;

    }

    /**
     * Get all tenant for a specific tenant.
     *
     *
     * @return Collection The collection of tenant for the given tenant.
     */
    public function getAll(): Collection
    {
        // Build a query for retrieving tenant, ordered by the latest, filtered by tenant_id
        $query = $this->tenant
            ->latest();

        $result = $query->get();

        return $result;
    }

}
