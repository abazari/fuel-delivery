<?php

namespace App\Repositories\Order;

use App\Models\Order;
use Illuminate\Pagination\LengthAwarePaginator;

class OrderRepository implements OrderRepositoryInterface
{
    public function __construct(protected Order $order)
    {

    }

    /**
     * Create a new order record.
     *
     * @param array $data The data for creating a order.
     *
     * @return Order The newly created order model instance.
     */
    public function createOrder(array $data): Order
    {
        return $this->order->create([
            'client_id' => $data['client_id'],
            'tenant_id' => $data['tenant_id'],
            'user_id' => $data['user_id'],
            'fuel_amount' => $data['fuel_amount'],
        ]);
    }


    /**
     * Get all orders for a specific order with optional pagination.
     *
     * @param int      $tenantId The ID of the tenant.
     * @param int|null $perPage  The number of items per page for pagination.
     *
     * @return LengthAwarePaginator The paginated result of orders for the given tenant.
     */
    public function getAllWithPagination(int $tenantId, ?int $perPage = null): LengthAwarePaginator
    {
        $query = $this->order->query()
            ->latest()
            ->where('tenant_id', $tenantId);

        $result = $query->paginate($perPage ?? config('config.default_paginate'));

        return $result;

    }

}