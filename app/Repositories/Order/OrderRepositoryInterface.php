<?php

namespace App\Repositories\Order;

use App\Models\Order;
use Illuminate\Pagination\LengthAwarePaginator;

interface OrderRepositoryInterface
{
    /**
     * Create a new order record.
     *
     * @param array $data The data for creating a order.
     *
     * @return Order The newly created order model instance.
     */
    public function createOrder(array $data): Order;

    /**
     * Get all orders for a specific tenant with optional pagination.
     *
     * @param int      $tenantId The ID of the tenant.
     * @param int|null $perPage  The number of items per page for pagination.
     *
     * @return LengthAwarePaginator The paginated result of orders for the given tenant.
     */
    public function getAllWithPagination(int $tenantId, ?int $perPage = null): LengthAwarePaginator;
}