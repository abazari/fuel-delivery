<?php

namespace App\Repositories\DeliveryTruck;

use App\Models\Cargo;
use App\Models\DeliveryTruck;
use App\Models\Tenant;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

interface DeliveryTruckRepositoryInterface
{
    /**
     * Get all delivery trucks for a specific tenant.
     *
     * @param int $tenantId The ID of the tenant.
     *
     * @return Collection The collection of delivery trucks for the given tenant.
     */
    public function getAll(int $tenantId): Collection;

    /**
     * Get all delivery trucks with optional pagination.
     *
     * @param int|null $perPage The number of items per page for pagination.
     *
     * @return LengthAwarePaginator The paginated result of all delivery trucks.
     */
    public function getAllWithPagination(int $tenantId,?int $perPage = null): LengthAwarePaginator;

    /**
     * Create a new delivery trucks record.
     *
     * @param array $data The data for creating a delivery trucks.
     *
     * @return DeliveryTruck The newly created delivery trucks model instance.
     */
    public function createDeliveryTruck(array $data): DeliveryTruck;

    /**
     * Find a delivery truck by its ID.
     *
     * @param int $id The ID of the truck to find.
     *
     * @return DeliveryTruck|null The found delivery truck or null if not found.
     */
    public function findTruckWithId(int $id): ?DeliveryTruck;
}
