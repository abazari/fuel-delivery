<?php

namespace App\Repositories\DeliveryTruck;

use App\Models\Cargo;
use App\Models\DeliveryTruck;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

/**
 * Class DeliveryTrucksRepository
 *
 * This class represents the repository for delivery trucks model operations.
 */
class DeliveryTruckRepository implements DeliveryTruckRepositoryInterface
{
    /**
     * ClientRepository constructor.
     *
     * @param DeliveryTruck $client The delivery truck model instance.
     */
    public function __construct(protected DeliveryTruck $deliveryTruck)
    {
    }

    /**
     * Get all delivery trucks for a specific delivery trucks.
     *
     * @param int $tenantId The ID of the tenant.
     *
     * @return Collection The collection of delivery trucks for the given delivery trucks.
     */
    public function getAll(int $tenantId): Collection
    {
        // Build a query for retrieving delivery trucks, ordered by the latest, filtered by tenant_id
        $query = $this->deliveryTruck
            ->latest()
            ->where('tenant_id', $tenantId);

        $result = $query->get();

        return $result;
    }

    /**
     * Get all delivery trucks with optional pagination.
     *
     * @param int|null $perPage The number of items per page for pagination.
     *
     * @return LengthAwarePaginator The paginated result of all delivery trucks.
     */
    public function getAllWithPagination(int $tenantId,?int $perPage = null): LengthAwarePaginator
    {
        $query = $this->deliveryTruck->query()
            ->latest()
            ->where('tenant_id',$tenantId);

        $result = $query->paginate($perPage ?? config('config.default_paginate'));

        return $result;

    }

    /**
     * Create a new delivery trucks record.
     *
     * @param array $data The data for creating a delivery trucks.
     *
     * @return DeliveryTruck The newly created delivery trucks model instance.
     */
    public function createDeliveryTruck(array $data): DeliveryTruck
    {
        return $this->deliveryTruck->create([
            'truck_name' => $data['truck_name'],
            'tenant_id' => $data['tenant_id'],
            'license_plate' => $data['license_plate'],
            'model' => $data['model'],
        ]);
    }

    /**
     * Find a delivery truck by its ID.
     *
     * @param int $id The ID of the truck to find.
     *
     * @return DeliveryTruck|null The found delivery truck or null if not found.
     */
    public function findTruckWithId(int $id): ?DeliveryTruck
    {

        $result = $this->deliveryTruck->where('id',$id)->first();

        return $result;
    }



}
