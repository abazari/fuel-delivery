<?php

namespace App\Traits;

use Illuminate\Support\Facades\Auth;
use PHPOpenSourceSaver\JWTAuth\Facades\JWTAuth;

trait LoginTrait
{
    /**
     * login with email password
     *
     * @param $request
     * @param string $guard
     * @return bool|string
     */
    public function attemptLogin($request, string $guard = 'user'): bool|string
    {
        $credentials = $request->only('email', 'password');

        $token = Auth::guard($guard)->attempt($credentials);

        if ($token) {
            return $token;
        }

        return false;
    }

    /**
     * Logout the user and invalidate the token.
     *
     * @param string $guardName
     * @return void
     */
    public function performLogout(string $guardName = 'user'): void
    {
        // Get the authenticated user.
        $user = Auth::guard($guardName)->user();

        if ($user) {
            // Invalidate the current token.
            JWTAuth::parseToken()->invalidate();
        }
    }

}
