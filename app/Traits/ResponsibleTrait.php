<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;

trait ResponsibleTrait
{
    /**
     * set response success
     *
     * @param $data
     * @param $message
     * @param int $status
     * @param array $header
     * @return JsonResponse
     *
     * @author Hossein Abazari <hossein.abazarikha@gmail.com>
     */
    public function success($message, $data = null, int $status = 200, array $header = [], $bodyStatus = 200): JsonResponse
    {
        $jsonData = [
            'message' => $message,
            'status' => $status,
        ];

        if ($data){
            $jsonData['result'] = $data;
        }

        return response()->json($jsonData, $bodyStatus, $header);
    }


    /**
     * set response error
     *
     * @param $message
     * @param int $status
     * @return JsonResponse
     *
     * @author Hossein Abazari <hossein.abazarikha@gmail.com>
     */
    public function error($message, int $status = 406): JsonResponse
    {
        return response()->json([
            'message' => $message,
            'errors' => true,
            'status' => $status,
        ]);
    }

}
