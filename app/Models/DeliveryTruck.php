<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DeliveryTruck extends Model
{
    use HasFactory;

    protected $fillable = [
        'tenant_id',
        'truck_name',
        'license_plate',
        'model',
    ];

    /**
     * @return BelongsTo
     */
    public function tenant(): BelongsTo
    {
        return $this->belongsTo(Tenant::class, 'tenant_id');
    }

    /**
     * @return HasMany
     */
    public function cargos(): HasMany
    {
        return $this->hasMany(Cargo::class, 'delivery_truck_id');
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
