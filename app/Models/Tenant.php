<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Tenant extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'domain',
    ];

    /**
     * @return HasMany
     */
    public function cargos(): HasMany
    {
        return $this->hasMany(Cargo::class, 'client_id');
    }

    /**
     * @return HasMany
     */
    public function deliveryTrucks(): HasMany
    {
        return $this->hasMany(DeliveryTruck::class, 'client_id');
    }

}
