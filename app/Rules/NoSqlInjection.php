<?php

namespace App\Rules;

use Closure;
use Illuminate\Contracts\Validation\ValidationRule;

class NoSqlInjection implements ValidationRule
{
    /**
     * Run the validation rule.
     *
     * @param  \Closure(string): \Illuminate\Translation\PotentiallyTranslatedString  $fail
     */
    public function validate(string $attribute, mixed $value, Closure $fail): void
    {
        $sql_keywords = ['select', 'update', 'delete', 'insert', 'drop', 'alter', 'truncate', 'union'];
        foreach ($sql_keywords as $keyword) {
            if (stripos($value, $keyword) !== false) {
                $fail(__('validation.no_sql_injection'));
            }
        }
    }
}
