<?php

namespace App\Providers;

use App\Repositories\Cargo\CargoRepository;
use App\Repositories\Cargo\CargoRepositoryInterface;
use App\Repositories\Client\ClientRepository;
use App\Repositories\Client\ClientRepositoryInterface;
use App\Repositories\DeliveryTruck\DeliveryTruckRepository;
use App\Repositories\DeliveryTruck\DeliveryTruckRepositoryInterface;
use App\Repositories\Order\OrderRepository;
use App\Repositories\Order\OrderRepositoryInterface;
use App\Repositories\Tenant\TenantRepository;
use App\Repositories\Tenant\TenantRepositoryInterface;
use App\Repositories\Transaction\TransactionRepository;
use App\Repositories\Transaction\TransactionRepositoryInterface;
use App\Repositories\User\UserRepository;
use App\Repositories\User\UserRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{

    protected array $binds = [
        [UserRepositoryInterface::class => UserRepository::class],
        [ClientRepositoryInterface::class => ClientRepository::class],
        [OrderRepositoryInterface::class => OrderRepository::class],
        [TenantRepositoryInterface::class => TenantRepository::class],
        [CargoRepositoryInterface::class => CargoRepository::class],
        [DeliveryTruckRepositoryInterface::class => DeliveryTruckRepository::class],

    ];
    /**
     * Register services.
     */
    public function register(): void
    {
        $this->bindAll($this->binds);
    }

    /**
     * Bootstrap services.
     */
    public function boot(): void
    {
        //
    }

    /**
     * bind All array class
     *
     * @return void
     */
    protected function bindAll($binds): void
    {
        foreach ($binds as $bind) {
            $this->app->bind(array_key_first($bind), array_values($bind)[0]);
        }
    }
}
